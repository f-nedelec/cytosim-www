---
title: ""
date: 2021-09-09T18:34:13+02:00
draft: false
---

Cytosim is a cytoskeleton simulation suite designed to handle large systems of flexible filaments with associated proteins such as molecular motors. It has been used to study actin and microtubule systems alike. It is built around a cross-platform core engine running in 2D or 3D. Cytosim offers command-line tools with simulation and display capabilities. With its modular code base, it can easily be customized to meet particular tasks. 

The simulation is specified in a [configuration file](doc/sim/config), defining objects with their parameters and operations, such as advancing time, saving frames or [generating reports](doc/sim/report). Below is a basic example, with parameters specified in [units of seconds, micrometers and picoNewtons](doc/sim/units).

```
	set simul system
	{
	    time_step = 0.005
	    viscosity = 0.02
	}
	
	set space cell
	{
	    shape = sphere
	}
	
	set fiber microtubule
	{
	    rigidity = 20
	    segmentation = 0.5
	    confine = inside, 200, cell
	}
	    
	new cell
	{
	    radius = 5
	}
	
	new 5 microtubule
	{
	    length = 11
	}
	
	run 5000 system
	{
	    nb_frames = 10
	}
```


The Brownian dynamics approach was described in:  
[Collective Langevin Dynamics of Flexible Cytoskeletal Fibers](http://iopscience.iop.org/article/10.1088/1367-2630/9/11/427/meta)

